using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    [SerializeField] private UIManager _uiManager;
    [SerializeField] private List<Level> _levels;

    private Level _currentLevel;

    private int _levelCount;

    public UnityEvent<string> OnItemListChange;

private void Start() 
{ 

    _uiManager.ShowStartScreen();
}
private void CreateLevel()
{
    int index = _levelCount;
    if (_currentLevel != null) 
    {
        Destroy(_currentLevel.gameObject);
        _currentLevel = null;
    }
    

    if (_levelCount >= _levels.Count)
    {
        _levelCount = 1;
        index = 1;
    }

    _currentLevel = Instantiate(_levels[index].gameObject).GetComponent<Level>();
}

public void StartGame ()
{
    CreateLevel();
    _currentLevel.OnComplete += StopGame;
    _currentLevel.OnItemListChanged += OnItemListChanged;
    _currentLevel.Initialaze();
    _uiManager.ShowGameScreen(_currentLevel.GetItemDictionary());
}

private void StopGame()
{
    _uiManager.ShowWinScreen();
    _levelCount++;
    _currentLevel.OnComplete -= StopGame;
    _currentLevel.OnItemListChanged -= OnItemListChanged;

}

public void ExitGame ()
{
    Application.Quit();
}

private void OnItemListChanged (string name) => OnItemListChange?.Invoke(name);
}
