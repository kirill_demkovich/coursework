using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(BoxCollider2D))]
public class GameItem : MonoBehaviour
{
    [SerializeField] private string _name;
    private SpriteRenderer _spriteRenderer;

    public string Name => _name;
    public Sprite Sprite => _spriteRenderer.sprite;
    private FindItem _findItem;
    public event Action<string> OnFind;

    private void Awake() {
        _findItem = GetComponent<FindItem>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    } 

    private void OnMouseUpAsButton() {
        OnFindItem();
    }

    private void OnFindItem()
    {
        _findItem.FindEffect();
        
            OnFind?.Invoke(_name);
            OnFind = null;
    }
}
