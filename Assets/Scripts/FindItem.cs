using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class FindItem : MonoBehaviour
{
    public abstract void FindEffect();
}
