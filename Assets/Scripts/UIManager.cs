using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private GameObject _startScreen;
    [SerializeField] private GameObject _winScreen;
    [SerializeField] private GameObject _gameScreen;  
    [SerializeField] private GameObject _title;

    private int _currentLevel;
    private GameObject _currentScreen;
    private UIGameScreen _uiGameScreen;


    private void Awake() 
    {
        _currentScreen = _startScreen;
        _uiGameScreen = _gameScreen.GetComponent<UIGameScreen>();
    }
public void ShowStartScreen()
{
_currentScreen.SetActive(false);
_startScreen.SetActive(true);
_currentScreen = _startScreen;
}

public void ShowGameScreen(Dictionary <string, GameItemData> ItemsData)
{
    _currentScreen.SetActive(false);
    _uiGameScreen.Initialaze(ItemsData);
    _gameScreen.SetActive(true);
    _currentScreen = _gameScreen;
}

public void ShowWinScreen()
{
_currentScreen.SetActive(false);
_currentLevel++;
_title.GetComponent<Text>().text = "Level №"+ _currentLevel.ToString() + "complete!";
_winScreen.SetActive(true);
_currentScreen = _winScreen;
}
}
