using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;

public class ScaleComponent : FindItem
{
    private int _scalecount = 1;

   public override void FindEffect()
   {
       transform.DOScale(transform.localScale * 3, _scalecount);
       transform.DORotate(new Vector3(0f,0f,180), _scalecount).OnComplete(() => 
       {
           gameObject.SetActive(false);
       });
   }


}
